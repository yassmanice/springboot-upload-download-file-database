package com.example.springboot.fileuploaddownload.service.repository;

import com.example.springboot.fileuploaddownload.model.DatabaseFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatabaseFileRepository extends JpaRepository<DatabaseFile, String> {

}